package jobs;
import java.util.List;
import java.util.Random;

import models.Comment;
import models.InitialMatchData;
import models.Post;
import models.Rating;
import models.Relation;
import models.Tag;
import models.UploadFile;
import models.User;
import play.Logger;
import play.db.Model;
import play.db.jpa.Blob;
import play.db.jpa.JPQL;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.modules.elasticsearch.ElasticSearch;
import play.test.Fixtures;
import tsmatch.Utils;
 
@OnApplicationStart
public class Bootstrap extends Job {
 
    public void doJob() {

        Logger.info("=================User Count = " + User.count());
        // 检查当前的数据库是否为空
        if(User.count() == 0) {
            
            // 清除Fixtures Cache，防止duplicate id 错误
            Fixtures.delete();
            Fixtures.loadModels("data.yml");
            Fixtures.loadModels("showing_data.yml");
            
            // 清除错误（不能与JPA正确映射）的索引
            reindexAll();
            
            // 设置超级用户
            User duanhong = (User)User.find("byEmail", "duanhong169@gmail.com").first();
            duanhong.addRole("admin");
            duanhong.addRole("superAdmin");
            duanhong.save();
            
            // 激活所有测试用户
            List<User> users = User.findAll();
            for(User user : users){
                user.confirmationCode = "";
                user.save();
            }
        }
        if(User.count()!=0 && UploadFile.count()==0){
            List<User> users = User.findAll();
            UploadFile testfile = UploadFile.findById(1L);
            Blob testblob = testfile.file;
            for(User user:users){
                for(int i = 0; i<4; i++){
                    new UploadFile(user, "测试文件" + i, testblob).save();
                }
            }
        }

        if(User.count()!=0 && Rating.count()<500){
            List<User> users = User.findAll();
            for(User user:users){
                for(int i = 0; i<100; i++){
                    int r = new Random().nextInt(10);
                    if(r<4) break;
                    new Rating(user.id,null,((UploadFile)UploadFile.findAll().get(i)).id,1 + (int)(Math.random() * 5),"").save();
                }
            }
        }
        
        if(User.count()!=0 && Relation.count()==0){
            
        }
        Logger.info("=================User Count = " + User.count());
        Logger.info("=================InitialMatchData Count = " + InitialMatchData.count());
        new RecommenderJob().now();
        new MatcherJob().now();
    }
    
    // 重新索引所有model
    public static void reindexAll(){
        ElasticSearch.client().admin().indices()
            .prepareDelete("_all")
            .execute().actionGet();
        Utils.customizeModels();
        updateModelIndex(Tag.class);
        updateModelIndex(Rating.class);
        updateModelIndex(UploadFile.class);
        updateModelIndex(Comment.class);
        updateModelIndex(Post.class);
        updateModelIndex(User.class);
    }
    
    // 根据JPA中对象重新建立索引
    private static <T extends Model> void updateModelIndex(Class<T> clazz) {

        List<T> objs = JPQL.instance.findAll(clazz.getName());
        for (T obj : objs) {
            ElasticSearch.index(obj);
        }
    }

}