package jobs;
import org.grouplens.lenskit.GlobalItemRecommender;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.RecommenderEngine;
import org.grouplens.lenskit.baseline.BaselinePredictor;
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor;
import org.grouplens.lenskit.core.LenskitRecommenderEngineFactory;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.knn.item.ItemItemGlobalRecommender;
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor;
import org.grouplens.lenskit.knn.item.ItemItemRecommender;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.MeanVarianceNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.VectorNormalizer;

import play.Logger;
import play.jobs.*;
 
@Every("1h")
public class RecommenderJob extends Job {
    
    private static RecommenderEngine recommenderEngine = null;
    boolean updating = false;
    
    public static RecommenderEngine getRecommenderEngine(){
        if(recommenderEngine != null){
            return recommenderEngine;
        } else {
            new RecommenderJob().now();
            return null;
        }
    }
    
    public void doJob() {
        
        if(updating) return;
    	Logger.info("==============Recommender Updating!===================");
    	updating = true;
        BasicSQLStatementFactory config = new BasicSQLStatementFactory();
        config.setIdColumn("id");
        config.setItemColumn("rated_file_id");
        config.setRatingColumn("rating_score");
        config.setTableName("rating");
        config.setTimestampColumn("ratedAt");
        config.setUserColumn("rater_id");
        JDBCRatingDAO.Factory daoFactory = 
                new JDBCRatingDAO.Factory("jdbc:mysql://localhost:3306/tsmatch?" +
                        "user=root&password=duanhong", config);
        LenskitRecommenderEngineFactory factory = 
                new LenskitRecommenderEngineFactory();
        factory.setDAOFactory(daoFactory);
        factory.bind(GlobalItemRecommender.class)
               .to(ItemItemGlobalRecommender.class);
        factory.bind(ItemRecommender.class)
               .to(ItemItemRecommender.class);
        factory.bind(ItemScorer.class)
               .to(ItemItemRatingPredictor.class);
        factory.bind(VectorNormalizer.class)
               .to(MeanVarianceNormalizer.class);
        factory.bind(BaselinePredictor.class)
               .to(ItemUserMeanPredictor.class);
        factory.bind(UserVectorNormalizer.class)
               .to(BaselineSubtractingUserVectorNormalizer.class);

        try {
            recommenderEngine = factory.create();
        } catch (RecommenderBuildException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        updating = false;
    }
    
}