package jobs;
import java.io.IOException;
import java.util.List;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;
import models.InitialMatchData;
import models.Relation;
import models.User;
import play.Logger;
import play.Play;
import play.jobs.*;
import tsmatch.Utils;
 
@Every("1h")
public class MatcherJob extends Job {
    
    public void doJob() {
        svm_problem matchProblem = buildSVMProblem();
        svm_parameter matchParameter = buildSVMParam(2048, 0.125);
        svm_model matchModel = svm.svm_train(matchProblem, matchParameter);
        try {
            svm.svm_save_model(Play.applicationPath + "/conf/match.model", matchModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public svm_parameter buildSVMParam(double c, double gamma){
        svm_parameter match_parameter = new svm_parameter();
        match_parameter.svm_type = svm_parameter.C_SVC;
        match_parameter.kernel_type = svm_parameter.RBF;
        match_parameter.degree = 3;
        match_parameter.gamma = gamma;
        match_parameter.coef0 = 0;
        match_parameter.nu = 0.5;
        match_parameter.cache_size = 100;
        match_parameter.C = c;
        match_parameter.eps = 0.001;
        match_parameter.p = 0.1;
        match_parameter.shrinking = 1;
        match_parameter.probability = 1;
        match_parameter.nr_weight = 0;
        return match_parameter;
    }
    
    public svm_problem buildSVMProblem(){
        svm_problem match_problem = new svm_problem();
        List<InitialMatchData> initial_matches = InitialMatchData.all().fetch();
        List<Relation> systemRelations = Relation.all().fetch();
        
        // 求出极值供标准化数据处理
        for(int i = 0; i<initial_matches.size(); i++ ){
            InitialMatchData data = initial_matches.get(i);
            double[] values = {data.mentorAge, data.mentorGender, 
                    Utils.level(data.mentorLevel),
                    Utils.yesno(data.mentorToHouse), data.mentorRating,
                    data.menteeAge, data.menteeGender,
                    Utils.yesno(data.menteeToHouse),
                    data.menteeRating,
                    Utils.kmdistance(data.mentorGeoPoint, data.menteeGeoPoint)};
            for(int j=0; j<values.length; j++){
                if(values[j] > Utils.maxValues[j]) Utils.maxValues[j] = values[j];
                if(values[j] < Utils.minValues[j]) Utils.minValues[j] = values[j];
            }
        }
        for(int i = 0; i<systemRelations.size(); i++){
            Relation relation = systemRelations.get(i);
            User mentor = relation.mentor;
            User mentee = relation.mentee;
            double[] values = {mentor.age,mentor.gender,Utils.level(mentor.level),
                    Utils.yesno(mentor.toHouse),mentor.rating_average,
                    mentee.age,mentee.gender,
                    Utils.yesno(mentee.toHouse),
                    mentee.rating_average,
                    Utils.kmdistance(mentor.geoPoint, mentee.geoPoint)};
            for(int j=0; j<values.length; j++){
                if(values[j] > Utils.maxValues[j]) Utils.maxValues[j] = values[j];
                if(values[j] < Utils.minValues[j]) Utils.minValues[j] = values[j];
            }
        }
        
        // 构造SVM_Problem
        match_problem.l = initial_matches.size() + systemRelations.size();
        match_problem.y = new double[match_problem.l];
        match_problem.x = new svm_node[match_problem.l][];
        for(int i = 0; i<initial_matches.size(); i++ ){
            InitialMatchData data = initial_matches.get(i);
//            Logger.info(data.mentorAge + ", " + data.mentorGender + ", " + data.mentorLevel
//                    + ", " + data.mentorToHouse + ", " + data.mentorRating + ", "
//                    + data.menteeAge + ", " + data.menteeGender + ", " + data.menteeToHouse
//                    + ", " + data.menteeRating + ", " + data.menteeGeoPoint + ", " + data.menteeGeoPoint);
            double[] values = {data.mentorAge, data.mentorGender, 
                    Utils.level(data.mentorLevel),
                    Utils.yesno(data.mentorToHouse), data.mentorRating,
                    data.menteeAge, data.menteeGender,
                    Utils.yesno(data.menteeToHouse),
                    data.menteeRating,
                    Utils.kmdistance(data.mentorGeoPoint, data.menteeGeoPoint)};
            values = Utils.scale(values);
            svm_node[] oneMatch = Utils.buildNode(values);
            match_problem.x[i] = oneMatch;
            match_problem.y[i] = data.relation;
        }
        for(int i = 0; i<systemRelations.size(); i++){
            Relation relation = systemRelations.get(i);
            User mentor = relation.mentor;
            User mentee = relation.mentee;
            double[] values = {mentor.age,mentor.gender,Utils.level(mentor.level),
                    Utils.yesno(mentor.toHouse),mentor.rating_average,
                    mentee.age,mentee.gender,
                    Utils.yesno(mentee.toHouse),
                    mentee.rating_average,
                    Utils.kmdistance(mentor.geoPoint, mentee.geoPoint)};
            values = Utils.scale(values);
            svm_node[] oneMatch = Utils.buildNode(values);
            match_problem.x[i+initial_matches.size()] = oneMatch;
            match_problem.y[i+initial_matches.size()] = relation.relationship;
        }
        
        Logger.info(match_problem.x[0][5].index + " | " + match_problem.x[0][5].value);
        Logger.info(match_problem.x[1][9].index + " | " + match_problem.x[1][9].value);
        Logger.info(match_problem.x[3][9].index + " | " + match_problem.x[3][9].value);
        Logger.info(match_problem.x[5][9].index + " | " + match_problem.x[5][9].value);
        
        return match_problem;
    }
    
}