package tsmatch;

import java.util.ArrayList;
import java.util.HashMap;

// 所有常量
public class Constants {

    // 用户类型代码
    public static final String TSMATCH_USERTYPE_MENTOR = "mentor";
    public static final String TSMATCH_USERTYPE_MENTEE = "mentee";
    public static final String TSMATCH_USERTYPE_AGENCY = "agency";
    
    // TODO: 参照用户类型进行修改
    public static final String TSMATCH_POSTTYPE_FIND_MENTOR = "ph";
    public static final String TSMATCH_POSTTYPE_FIND_MENTEE = "ph";

}
