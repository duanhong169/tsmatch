package tsmatch;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jobs.RecommenderJob;

import libsvm.svm_node;
import models.Rating;

import org.elasticsearch.action.admin.cluster.state.ClusterStateResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.client.Requests;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.grouplens.lenskit.GlobalItemRecommender;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.RecommenderEngine;
import org.grouplens.lenskit.baseline.BaselinePredictor;
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor;
import org.grouplens.lenskit.core.LenskitRecommenderEngineFactory;
import org.grouplens.lenskit.data.dao.EventCollectionDAO;
import org.grouplens.lenskit.data.event.SimpleRating;
import org.grouplens.lenskit.data.sql.BasicSQLStatementFactory;
import org.grouplens.lenskit.data.sql.JDBCRatingDAO;
import org.grouplens.lenskit.data.sql.SQLStatementFactory;
import org.grouplens.lenskit.knn.item.ItemItemGlobalRecommender;
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor;
import org.grouplens.lenskit.knn.item.ItemItemRecommender;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

import play.Logger;
import play.modules.elasticsearch.ElasticSearch;

public class Utils {
    
    public static svm_node[] buildNode(double[] values){
        svm_node[] nodes = new svm_node[values.length];
        for(int i=0; i < nodes.length; i++){
            nodes[i] = new svm_node();
        }
        for(int i=0; i < values.length; i++){
            nodes[i].index = i + 1;
            nodes[i].value = values[i];
        }
        return nodes;
    }
    
    public static Recommender getRecommender(){
//        List<Rating> ratings = Rating.find("select rating from Rating rating " +
//                "where rating.rated_file_id is not null").fetch();
//        ArrayList<SimpleRating> ratings_forRecSys = new ArrayList();
//        for(Rating rating : ratings){
//            ratings_forRecSys.add(rating.forRecSys());
//        }
//        EventCollectionDAO.Factory daoFactory = new EventCollectionDAO.Factory(ratings_forRecSys);
        RecommenderEngine recommenderEngine = RecommenderJob.getRecommenderEngine();
        if(recommenderEngine == null) return null;
        return recommenderEngine.open();
    }
    
    public static double yesno(boolean toHouse){
        if(toHouse == true) return 1;
        else if(toHouse == false) return -1;
        else return 0;
    }

    public static double level(String level){
        if(level.equals("高中")) return 1.0;
        else if(level.equals("专科")) return 2.0;
        else if(level.equals("本科")) return 3.0;
        else if(level.equals("研究生")) return 4.0;
        else return 0;
    }
    
    public static double kmdistance(String geoPoint1, String geoPoint2){
        float lat1 = Float.parseFloat(geoPoint1.split(",")[0]);
        float long1 = Float.parseFloat(geoPoint1.split(",")[1]);
        float lat2 = Float.parseFloat(geoPoint2.split(",")[0]);
        float long2 = Float.parseFloat(geoPoint2.split(",")[1]);
        double latdif = 111.1*(lat2-lat1);
        double longdif = 85.2*(long2-long1);
        return Math.pow((latdif*latdif+longdif*longdif),0.5);
    }
    
    public static double[] minValues = {22,0,1,-1,3,10,0,-1,3,5};
    public static double[] maxValues = {40,1,4,1,5,18,1,1,5,20};
    
    public static double[] scale(double[] rawdata){
        if(rawdata.length != 10) return rawdata;
        double[] scaleddata = rawdata;
        for(int i=0; i<scaleddata.length; i++){
            scaleddata[i] = (rawdata[i] - minValues[i])/(maxValues[i] - minValues[i]);
            if(scaleddata[i] < 0) scaleddata[i] = 0;
            if(scaleddata[i] > 1) scaleddata[i] = 1;
        }
        return scaleddata;
    }
    
    public static List sqlResultSetToList(ResultSet rs) throws SQLException{
        List resultList = new ArrayList<Map>();
        ResultSetMetaData rsmd = rs.getMetaData();
        int count = rsmd.getColumnCount();
        while (rs.next()) {
            HashMap map = new HashMap();
            for (int i = 0; i < count; i++) {
                String columnName = rsmd.getColumnName(i+1);
                int sqlType = rsmd.getColumnType(i+1);
                Object sqlView = rs.getString(columnName);
                if (Types.CHAR == sqlType && null != sqlView) {
                    map.put(columnName, sqlView.toString().trim());
                } else {
                    map.put(columnName, sqlView);
                } 
            }
            resultList.add(map);
        }
        return resultList;
    }
    
    public static void customizeModels(){
        /**
         * 主要用于熟悉ElasticSearch Java API的使用
         * @author duanhong169@gmail.com
         */
        try {
            
            // 由于要自定义models_user的索引方式，因此首先判断是否已经建立了models_user索引
            ElasticSearch.client().admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet(); 
            ClusterStateResponse response = 
                    ElasticSearch.client().admin().cluster().prepareState().execute().actionGet(); 
            boolean hasIndex = response.getState().metaData().hasIndex("models_user"); 
            
            
            if(!hasIndex) {
                Logger.info("+++++++++++new models_user index++++++++++++");
                // 如果models_user尚未被创建，则执行第一次建立索引时的配置设置
                ElasticSearch.client().admin().indices().prepareCreate("models_user")
                    .setSettings(ImmutableSettings.settingsBuilder().loadFromSource(XContentFactory.jsonBuilder()
                        .startObject()
//                          .startObject("analysis")
//                              .startObject("analyzer")
//                                  .startObject("default")
//                                      .field("type", "SmartChinese")
//                                  .endObject()
//                              .endObject()
//                          .endObject()
                        .endObject().string()))
                    .execute().actionGet();
                
                // 自定义需要特殊处理的字段，该处则是将用户类型名称的索引设置为not_analyzed，即不解析该字段
                XContentBuilder mapping;
                mapping = XContentFactory.jsonBuilder()  
                        .startObject()  
                            .startObject("models_user")  
                                .startObject("properties")         
                                    .startObject("userType")
                                        .field("type", "string")
                                        .field("index", "not_analyzed")
                                    .endObject()
                                    .startObject("level")
                                        .field("type", "string")
                                        .field("index", "not_analyzed")
                                    .endObject()
                                    .startObject("district")
                                        .field("type", "string")
                                        .field("index", "not_analyzed")
                                    .endObject()
                                .endObject()  
                            .endObject()  
                        .endObject();
                PutMappingRequest mappingRequest = Requests.putMappingRequest("models_user").type("models_user").source(mapping);
                ElasticSearch.client().admin().indices().putMapping(mappingRequest).actionGet();
            } else {
                Logger.info("|||||||||||update models_user index||||||||||");
                // 如果models_user已经存在，则执行更新索引设置
//              ElasticSearch.client().admin().indices().prepareUpdateSettings((ImmutableSettings.settingsBuilder().loadFromSource(XContentFactory.jsonBuilder()
//                      .startObject()
//                          .startObject("analysis")
//                              .startObject("analyzer")
//                                  .startObject("default")
//                                      .field("type", "SmartChinese")//使用SmartChinese作为analyzer
//                                  .endObject()
//                              .endObject()
//                          .endObject()
//                      .endObject().string())).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }  
    }
}
