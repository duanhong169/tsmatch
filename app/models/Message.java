package models;
 
import java.util.*;

import javax.persistence.*;
 
import play.db.jpa.*;
import play.data.validation.*;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;

// 站内信 Private Message
@Entity
public class Message extends Model {
 
    @Required
    public Date time;
    
    // 消息发送目标用户
    @ManyToOne
    @Required
    public User to;
    
    // 发送消息的用户
    @ManyToOne
    @Required
    public User from;
    
    @Lob
    @Required
    @MaxSize(500)
    public String content;
    
    // XXX: 坑爹啊, 不能用read作为列名
    @Required
    public long has_read;
    
    public Message(String content, User to, User from) {
        this.time = new Date();
        this.to = to;
        this.from = from;
        this.content = content;
        this.has_read = 0;
    }
 
    public String toString() {
        return from.username + "发送给" + to.username + "的消息: " + content;
    }
}