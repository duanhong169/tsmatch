package models;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import controllers.CRUD.Exclude;

import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.modules.elasticsearch.annotations.ElasticSearchEmbedded;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;
import play.modules.elasticsearch.annotations.ElasticSearchable;

@ElasticSearchable
@Entity
public class Post extends Model {
 
    @Required
    public String title;
    
    @Required
    public Date postedAt;
    
    @Lob
    @Required
    @MaxSize(10000)
    public String content;
    
    @Required
    public String requestType;
    
    @Required
    @ManyToOne
    @ElasticSearchEmbedded(fields={"username"})
    public User author;
    
//    @OneToMany(mappedBy="belongsto", cascade=CascadeType.ALL)
//    @ElasticSearchIgnore
//    public List<UploadFile> attachments;
    
    @OneToMany(mappedBy="post", cascade=CascadeType.ALL)
    @ElasticSearchIgnore
    public List<Comment> comments;
    
    @ElasticSearchIgnore
    @ManyToMany(cascade=CascadeType.PERSIST)
    public Set<Tag> tags;
    
    public Post(User author, String title, String requestType, String content) {
        this.comments = new ArrayList<Comment>();
        this.requestType = requestType;
        this.tags = new TreeSet<Tag>();
        this.author = author;
        this.title = title;
        this.content = content;
        this.postedAt = new Date();
        //this.attachments = new ArrayList<UploadFile>();
    }
    
    public Post addComment(String author, String content) {
        Comment newComment = new Comment(this, author, content).save();
        this.comments.add(newComment);
        this.save();
        return this;
    }
    
    // TODO: 博文可以添加附件
//    public Post addAttachment(String author, String content) {
//        UploadFile newAttachment = new UploadFile(this, author, content).save();
//        this.comments.add(newComment);
//        this.save();
//        return this;
//    }
    
    public Post previous() {
        return Post.find("postedAt < ? order by postedAt desc", postedAt).first();
    }
     
    public Post next() {
        return Post.find("postedAt > ? order by postedAt asc", postedAt).first();
    }
    
    public Post tagItWith(String name) {
        tags.add(Tag.findOrCreateByName(name));
        return this;
    }
    
    public static List<Post> findTaggedWith(String tag) {
        return Post.find(
            "select distinct p from Post p join p.tags as t where t.name = ?", tag
        ).fetch();
    }
    
    public static List<Post> findTaggedWith(String... tags) {
        return Post.find(
            "select distinct p from Post p join p.tags as t where t.name in (:tags) " +
            "group by p.id, p.author, p.title, p.content, p.postedAt having count(t.id) = :size"
        ).bind("tags", tags).bind("size", tags.length).fetch();
    }
    
    public String toString() {
        return title;
    }
}