package models;
 
import java.util.*;

import javax.persistence.*;
 
import play.db.jpa.*;
import play.data.validation.*;

@Entity
public class CreditLog extends Model {
 
    @Required
    public Date time;
    
    @Required
    public String description;
    
    @Required
    public long amount;
 
    // 该次积分收支的用户
    @ManyToOne
    @Required
    public User owner;
    
    public CreditLog(String description, long amount, User owner) {
        this.description = description;
        this.amount = amount;
        this.owner = owner;
        this.time = new Date();
    }
 
    public String toString() {
        return description + "--积分" + amount + "--用户" + owner.username;
    }

}