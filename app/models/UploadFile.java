package models;

import java.io.File;
import java.util.*;

import javax.persistence.*;

import controllers.CRUD.Exclude;
 
import play.db.jpa.*;
import play.data.validation.*;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;
import play.modules.elasticsearch.annotations.ElasticSearchable;

// 代表一个上传文件
@ElasticSearchable
@Entity
public class UploadFile extends Model {
 
    @Required
    public String filename;
    
    @Required
    public Date uploadedAt;
    
    @ElasticSearchIgnore
    public Blob file;
    
    // 该文件的所有者
    @ManyToOne
    @ElasticSearchIgnore
    public User owner;
    
    @Lob
    @Required
    @MaxSize(10000)
    public String description;
    
    // 文件分数（平均分）
    @Exclude
    public double rating_average;
    
    // 文件评价总分（累计和）
    @Exclude
    public double rating_sum;
    
    // 文件被评价的次数
    @Exclude
    public long rating_amount;
 
    public UploadFile(User owner, String filename, Blob file) {
        this.owner = owner;
        this.filename = filename;
        this.file = file;
        this.description = "该文件暂无描述。";
        this.rating_amount = 0;
        this.rating_average = 0;
        this.rating_sum = 0;
        this.uploadedAt = new Date();
    }
 
    public UploadFile addReceivedRating(Rating newRating) {
        this.rating_amount++;
        this.rating_sum += newRating.rating_score;
        this.rating_average = rating_sum / rating_amount;
        this.save();
        return this;
    }
    
    public String toString() {
        return filename;
    }      
}