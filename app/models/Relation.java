package models;
 
import java.util.*;
import javax.persistence.*;

import org.grouplens.lenskit.data.event.SimpleRating;
 
import play.db.jpa.*;
import play.data.validation.*;
import play.modules.elasticsearch.annotations.ElasticSearchEmbedded;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;
import play.modules.elasticsearch.annotations.ElasticSearchable;

// 代表一次匹配
@Entity
public class Relation extends Model {

    // 学生用户
    @Required
    @ManyToOne
    public User mentee;

    // 教师用户
    @Required
    @ManyToOne
    public User mentor;
    
    // 该次关系更新的时间
    @Required
    public Date updatedAt;
    
    // 最新匹配关系
    @Required
    public int relationship;
    
    public Relation(User mentor, User mentee, int relationship) {
        this.mentee = mentee;
        this.mentor = mentor;       
        this.relationship = relationship;
        this.updatedAt = new Date();
    }
    
    public String toString() {
        return mentor.username + "匹配-" + mentee.username + "与" + relationship + "时间-" + updatedAt;
    }
}