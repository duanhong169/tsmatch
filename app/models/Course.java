package models;
 
import java.util.*;

import javax.persistence.*;
 
import play.db.jpa.*;
import play.data.validation.*;

@Entity
public class Course extends Model {

    @Required
    public String courseName;
    
    public String description;
    
    public Course(String courseName, String description) {
        this.courseName = courseName;
        this.description = description;
    }
 
    public String toString() {
        return courseName + ": " + description;
    }

}