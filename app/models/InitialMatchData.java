package models;

import javax.persistence.Entity;

import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class InitialMatchData extends Model {

    @Required
    public int mentorAge;
    
    @Required
    public int mentorGender;
    
    @Required
    public String mentorLevel;
    
    @Required
    public boolean mentorToHouse;
    
    @Required
    public double mentorRating;
    
    @Required
    public String mentorGeoPoint;
    
    @Required
    public int menteeAge;
    
    @Required
    public int menteeGender;
    
    @Required
    public boolean menteeToHouse;
    
    @Required
    public double menteeRating;
    
    @Required
    public String menteeGeoPoint;
    
    @Required
    public int relation;
}
