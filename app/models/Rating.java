package models;
 
import java.util.*;
import javax.persistence.*;

import org.grouplens.lenskit.data.event.SimpleRating;
 
import play.db.jpa.*;
import play.data.validation.*;
import play.modules.elasticsearch.annotations.ElasticSearchEmbedded;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;
import play.modules.elasticsearch.annotations.ElasticSearchable;

// 投票, 代表一次投票
@ElasticSearchable
@Entity
public class Rating extends Model {

    // 该次投票的发起者
    @Required
    @ElasticSearchIgnore
    public Long rater_id;
    
    // 该次投票的时间
    @Required
    public Date ratedAt;
    
    // 该次投票的分数
    @Required
    public int rating_score;
    
    // 该次投票的评论内容
    @Required
    @MaxSize(100)
    public String content;
    
    // 该次投票的目标用户
    @ElasticSearchIgnore
    public Long rated_user_id;
    
    @ElasticSearchIgnore
    public Long rated_file_id;
    
    public Rating(Long rater_id, Long rated_user_id, Long rated_file_id, int score, String content) {
        this.rater_id = rater_id;
        this.rated_user_id = rated_user_id;
        this.rated_file_id=rated_file_id;
        this.rating_score = score;
        this.content = content;
        this.ratedAt = new Date();
    }
    
    public String toString() {
        return rater_id + " Rated at: " + ratedAt;
    }
    
    public SimpleRating forRecSys(){
        return new SimpleRating(0, rater_id, rated_file_id, rating_score);
    }
    
    public User getRater(){
        return User.findById(rater_id);
    }
}