package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import controllers.CRUD.Exclude;

import play.data.Upload;
import play.data.validation.Required;
import play.db.jpa.Blob;
import play.db.jpa.Model;
import play.modules.elasticsearch.annotations.ElasticSearchIgnore;
import play.modules.elasticsearch.annotations.ElasticSearchable;
import casino.Casino;

@ElasticSearchable
@Entity
public class User extends Model {
    
    // 账号注册信息开始 ↓
    public String email;
    
    public String username;
    
    @Required
    public Date createdAt;
    
    @ElasticSearchIgnore
    public String pwHash;
    
    // 使得可以通过initial-data为User设定密码，因为实际上在数据库中保存的是密码的Hash值
    public void setPassword(String password) {
        this.pwHash = Casino.getHashForPassword(password);
    }
    
    @ElasticSearchIgnore
    public String confirmationCode;
    
    @ElasticSearchIgnore
    public String recoverPasswordCode;

    @Exclude
    @ElasticSearchIgnore
    private ArrayList<String> roles;
    // ↑ 账号注册信息截止
    
    // 用户信息（UserInfo）开始
    
    @Required
    public int age;
    
    @Required
    public int gender;
    
    @Required
    public String address;
    
    @Required
    public long districtCode;
    
    @Exclude
    public String geoPoint;
    
    @Required
    public boolean toHouse;
    
    @Required
    public String userType;
    
    public String level;
    
    public String district;
    
    public long credits;

    
    @Exclude
    @ElasticSearchIgnore
    public Blob avatar;
    
    @Exclude
    @OneToMany(mappedBy="owner", cascade=CascadeType.ALL)
    @ElasticSearchIgnore
    public List<UploadFile> files;
    
    @Exclude
    public ArrayList<String> services;
    
    // 用户分数（平均分）
    @Exclude
    public double rating_average;
    
    // 用户评价总分（累计和）
    @Exclude
    public double rating_sum;
    
    // 用户被评价的次数
    @Exclude
    public long rating_amount;
    
    public User(String email, String username, String passwordHash, String confirmationCode) {
        this.email = email;
        this.username = username;
        this.createdAt = new Date();
        this.pwHash = passwordHash;
        this.confirmationCode = confirmationCode;
        this.roles = new ArrayList<String>();
        this.age = 20;
        this.gender = 1;
        this.address= "地址尚未设置";
        this.userType = "mentee";
        this.level = "高中";
        this.district = "510111";//高新西区
        this.credits = 20;//用户初始积分——20分
        this.geoPoint = "30.6587,104.0648";//默认Google Map地理位置——成都
        this.toHouse = true;
        this.avatar = null;//默认头像
        this.files = new ArrayList<UploadFile>();
        this.services = new ArrayList<String>();
        this.rating_amount = 0;
        this.rating_average = 0;//用户初始评分——0分
        this.rating_sum = 0;//用户初始评分——0分
    }

    public String toString() {
        return email;
    }

    public boolean hasRole(String role) {
        HashSet<String> rolesSet = new HashSet<String>(getRoles());
        return rolesSet.contains(role);
    }

    public void addRole(String role) {
        HashSet<String> rolesSet = new HashSet<String>(getRoles());
        rolesSet.add(role);
        getRoles().clear();
        getRoles().addAll(new ArrayList<String>(rolesSet));
    }

    public void removeRole(String role) {
        HashSet<String> rolesSet = new HashSet<String>(getRoles());
        rolesSet.remove(role);
        getRoles().clear();
        getRoles().addAll(new ArrayList<String>(rolesSet));
    }
    
    public List<String> getRoles() {
        if (roles == null) {
            roles = new ArrayList<String>();
        }
        return roles;
    }
    
    public boolean hasService(String service) {
        HashSet<String> servicesSet = new HashSet<String>(getServces());
        return servicesSet.contains(service);
    }

    public void addService(String serviceName) {
        HashSet<String> servicesSet = new HashSet<String>(getServces());
        servicesSet.add(serviceName);
        getServces().clear();
        getServces().addAll(new ArrayList<String>(servicesSet));
    }

    public void removeService(String serviceName) {
        HashSet<String> servicesSet = new HashSet<String>(getServces());
        servicesSet.remove(serviceName);
        getServces().clear();
        getServces().addAll(new  ArrayList<String>(servicesSet));
    }
    
    public List<String> getServces() {
        if (services == null) {
            services = new ArrayList<String>();
        }
        return services;
    }
    
    public User addFile(String filename, Blob file) {
        UploadFile newAttachment = new UploadFile(this, filename, file).save();
        this.files.add(newAttachment);
        this.save();
        return this;
    }
    
    public User addReceivedRating(Rating newRating) {
        this.rating_amount++;
        this.rating_sum += newRating.rating_score;
        this.rating_average = rating_sum / rating_amount;
        this.save();
        return this;
    }
    
    public void changeCredits(String description, long amount) {
        if(credits + amount >= 0) {
            new CreditLog(description, amount, this).save();
            this.credits += amount;
            this.save();
        }
    }   
}