package controllers;

import java.util.List;

import models.Post;
import models.Tag;
import models.User;
import controllers.CRUD.ObjectType;
import controllers.CRUDs.CRUDPosts;
import controllers.casino.Security;
import play.mvc.Before;
import play.mvc.Controller;
import tsmatch.Constants;

// Yabe
public class Blogs extends Controller {
    
    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            User user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user);
        }
    }

    // 我的博客首页
    public static void index() {
        String user = Security.connected();
        List<Post> posts = Post.find("author.email", user).fetch();
        render(posts);
    }
    
    // 用于发布或修改一篇博文的表单
    public static void form(Long id) {
        ObjectType type = ObjectType.get(Blogs.class);
        renderArgs.put("type", type);
        if(id != null) {
            Post post = Post.findById(id);
            render(post);
        }
        render();
    }
    
    // 保存修改
    public static void save(Long id, String title, String requestType, String content, String tags) {
        Post post;
        if(id == null) {
            // Create post
            User author = User.find("byEmail", Security.connected()).first();
            post = new Post(author, title, requestType, content);
        } else {
            // Retrieve post
            post = Post.findById(id);
            // Edit
            post.title = title;
            post.requestType = requestType;
            post.content = content;
            post.tags.clear();
        }
        // Set tags list
        for(String tag : tags.split("\\s+")) {
            if(tag.trim().length() > 0) {
                post.tags.add(Tag.findOrCreateByName(tag));
            }
        }
        // Validate
        validation.valid(post);
        if(validation.hasErrors()) {
            render("@form", post);
        }
        // Save
        post.save();
        index();
    }
}
