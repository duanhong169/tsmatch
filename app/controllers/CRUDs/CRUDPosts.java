package controllers.CRUDs;
 
import models.Post;
import controllers.CRUD;
import controllers.CRUD.For;
import controllers.casino.Check;
import controllers.casino.Secure;
import play.*;
import play.mvc.*;
import tsmatch.Constants;

@Check("role:superAdmin")
@With(Secure.class)
@CRUD.For(Post.class)
public class CRUDPosts extends CRUD {    
}