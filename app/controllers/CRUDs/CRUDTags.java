package controllers.CRUDs;
 
import models.Tag;
import controllers.CRUD;
import controllers.CRUD.For;
import controllers.casino.Check;
import controllers.casino.Secure;
import play.*;
import play.mvc.*;
import tsmatch.Constants;
 
@Check("role:superAdmin")
@With(Secure.class)
@CRUD.For(Tag.class)
public class CRUDTags extends CRUD {    
}