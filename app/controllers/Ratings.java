package controllers;

import models.Post;
import models.Rating;
import models.UploadFile;
import models.User;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import tsmatch.Constants;
import controllers.casino.Secure;
import controllers.casino.Security;

@With(Secure.class)
public class Ratings extends Controller {
    
    private static User currentUser;

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    /**
     * 执行一次投票操作
     * @param userId 被评价的用户ID
     * @param content 评价内容
     * @param score 评价分数
     * @param code 验证码
     * @param randomID 验证码ID
     */
    public static void rate(
            Long userId,
            Long fileId,
            @Required(message="评论内容不能为空") @MaxSize(message="超过字数限制，请编辑内容后重新提交",value=100)String content,
            @Required(message="请评分") int score,
            @Required(message="请输入验证码") String code,
            String randomID)
    {
        System.out.println(userId);
        if(userId!=null){
            Users.showInfo(userId);
            //TODO: 不能评价自己(提示信息)
            if(userId == currentUser.id) return; 
        }

        if(!Play.id.equals("test")) {
            if(Cache.get(randomID) == null){
                validation.addError("captchaInvalidate", "验证码已过期，请重新输入");
            }else{
                validation.equals(code.toLowerCase(), 
                        Cache.get(randomID)
                        .toString()
                        .toLowerCase())
                        .message("错误的验证码");
            }
        }else{
            // we are in test mode.. ignore wrong captcha
            if (validation.errors().size() == 1) {

                if (validation.errors().get(0).getKey().equals("code")) {
                    validation.clear();
                }
            }
        }
        if(validation.hasErrors()) {
            validation.keep();
            if(userId!=null) Users.showInfo(userId);
            if(fileId!=null) UploadFiles.showdetail(fileId);
        }
        Rating newRating = new Rating(currentUser.id, userId, fileId, score, content).save();
        if(userId!=null) ((User)User.findById(userId)).addReceivedRating(newRating);
        if(fileId!=null) {
            ((UploadFile)UploadFile.findById(fileId)).addReceivedRating(newRating);
        }
        flash.put("rate_success", "评价成功！谢谢您的反馈，" + currentUser.username + "。");
        Cache.delete(randomID);
        if(userId!=null) Users.showInfo(userId);
        if(fileId!=null) UploadFiles.showdetail(fileId);
    }
}
