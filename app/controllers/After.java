package controllers;

import org.junit.Test;

import play.cache.Cache;
import play.mvc.Http.Response;
import casino.AfterUserCreationHook;
import casino.Casino;

public class After implements AfterUserCreationHook {

    public static String lastEmail = "";
    public static int executed = 0;

    /**
     * 用户注册后需要执行的操作
     */
    @Override
    public void execute(String email) {

        lastEmail = email;
        executed++;
        System.out.println(lastEmail);
        System.out.println(executed);
    }
}
