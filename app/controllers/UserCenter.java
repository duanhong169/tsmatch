package controllers;
 
import play.*;
import play.cache.Cache;
import play.mvc.*;
import tsmatch.Constants;
import tsmatch.Utils;
 
import java.io.IOException;
import java.util.*;

import org.grouplens.lenskit.GlobalItemRecommender;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.RecommenderEngine;
import org.grouplens.lenskit.baseline.BaselinePredictor;
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor;
import org.grouplens.lenskit.core.LenskitRecommenderEngineFactory;
import org.grouplens.lenskit.data.dao.EventCollectionDAO;
import org.grouplens.lenskit.data.event.SimpleRating;
import org.grouplens.lenskit.knn.item.ItemItemGlobalRecommender;
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor;
import org.grouplens.lenskit.knn.item.ItemItemRecommender;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

import controllers.CRUD.ObjectType;
import controllers.casino.Secure;
import controllers.casino.Security;
 
import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import models.*;
 
@With(Secure.class)
public class UserCenter extends Controller {
    
    private static User user;
    
    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            user = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", user);
        }
    }

    // 用户中心首页
    public static void index() throws IOException {
        List<User> matched_users = getmatchlist();
        List<Long> recList = getreclist(8);
        List<UploadFile> rec_files = new ArrayList<UploadFile>();
        for(Long fileid:recList){
            rec_files.add((UploadFile) UploadFile.findById(fileid));
        }
        render(matched_users, rec_files);
    }
    
    public static void matchlist(long id) throws IOException{
        List<User> matched_users = getmatchlist();
        render(matched_users);
    }
    
    public static List<User> getmatchlist() throws IOException{
        List<User> matchedTopUsers = Cache.get("matchlist_" + user.id, ArrayList.class);
        if(matchedTopUsers != null) {
            Logger.info("====================Using matchlist cache!==================");
            return matchedTopUsers;
        }
        final class ValueUserPair implements Comparable<ValueUserPair>{
            double value;
            User user;
            
            ValueUserPair(double value, User user){
                this.user = user;
                this.value = value;
            }
            
            @Override
            public int compareTo(ValueUserPair o) {
                if(value < o.value) return 1;
                else if(value > o.value) return -1;
                else return 0;
            }
        }
        //User user = User.findById(id);
        List<ValueUserPair> matchedResults = new ArrayList<ValueUserPair>();
        if(user.userType.equals("mentee")){
            List<User> mentors = User.find("select user from User user " +
                    "where user.userType = ? and user not in(select relation.mentor from Relation relation " +
                    "where relation.mentee = ?)", Constants.TSMATCH_USERTYPE_MENTOR, user).fetch();
            Logger.info("total " + mentors.size() + "mentors");
            for(User mentor:mentors){
                boolean course_matched = false;
                for(String course: mentor.getServces()){
                    if(user.hasService(course)) course_matched = true;
                }
                if(!course_matched) continue;
                double answer = predict(mentor, user);
                if(answer > 0){
                    ValueUserPair newPair = new ValueUserPair(answer, mentor);
                    matchedResults.add(newPair);
                }
            }
        }
        if(user.userType.equals("mentor")){
            List<User> mentees = User.find("select user from User user " +
                    "where user.userType = ? and user not in(select relation.mentee from Relation relation " +
                    "where relation.mentor = ?)", Constants.TSMATCH_USERTYPE_MENTEE, user).fetch();
            Logger.info("total " + mentees.size() + "mentees");
            for(User mentee:mentees){
                boolean course_matched = false;
                for(String course: mentee.getServces()){
                    if(user.hasService(course)) course_matched = true;
                }
                if(!course_matched) continue;
                double answer = predict(user, mentee);
                if(answer > 0){
                    ValueUserPair newPair = new ValueUserPair(answer, mentee);
                    matchedResults.add(newPair);
                }
            }
        }
        Collections.sort(matchedResults);
        //List<Long> answers = new ArrayList<Long>();
        matchedTopUsers = new ArrayList<User>();
        // 返回最匹配的前四位用户id
        for(int i = 0; i<(matchedResults.size() > 4 ? 4 : matchedResults.size()); i++){
            //answers.add(matchedResults.get(i).user.id);
            matchedTopUsers.add(matchedResults.get(i).user);
            Logger.info(i + ":" + matchedResults.get(i).value);
        }
        Cache.set("matchlist_" + user.id, matchedTopUsers, "1h");
        //renderJSON(answers);
        return matchedTopUsers;
    }
    
    public static double predict(User mentor, User mentee) throws IOException{
        svm_model matchModel = svm.svm_load_model(Play.applicationPath + 
                                                  "/conf/match.model");
        double[] probability = new double[matchModel.nr_class];
        double[] values = {mentor.age,mentor.gender,Utils.level(mentor.level),
                            Utils.yesno(mentor.toHouse),mentor.rating_average,
                            mentee.age,mentee.gender,
                            Utils.yesno(mentee.toHouse),
                            mentee.rating_average,
                            Utils.kmdistance(mentor.geoPoint, mentee.geoPoint)};
        values = Utils.scale(values);
        svm_node[] oneMatch = Utils.buildNode(values);
        double answer = svm.svm_predict_probability(matchModel, 
                                          oneMatch, probability);
        if(answer==1.0) answer=probability[0]; //返回匹配的可能性，方便排序
        return answer;
    }
    
    public static void reclist(long id) throws IOException{
        List<Long> recList = getreclist(16);
        List<UploadFile> rec_files = new ArrayList<UploadFile>();
        for(Long fileid:recList){
            rec_files.add((UploadFile) UploadFile.findById(fileid));
        }
        render(rec_files);
    }
    
//  Set<Long> exclude = Rating.find("select distinct rating.rated_file_id from Rating rating " +
//  "where rating.rater_id = ? and rating.rated_file_id is not null", user.id).first();
//List<Long> answers = irec.recommend(user.id,num,null,exclude);
    
    public static List<Long> getreclist(int num){
        
        Recommender rec = Utils.getRecommender();
        if(rec == null) return new ArrayList();
        ItemRecommender irec = null;
        try {
          irec = rec.getItemRecommender();
          if(irec == null) return new ArrayList();
          List<Long> answers = irec.recommend(user.id,num);
          return answers;
        } finally {
           rec.close();
        }
    }
    
}