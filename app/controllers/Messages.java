package controllers;


import java.util.List;

import models.CreditLog;
import models.Message;
import models.User;
import play.Logger;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import tsmatch.Constants;
import controllers.casino.Secure;
import controllers.casino.Security;

@With(Secure.class)
public class Messages extends Controller {
    
    private static User currentUser;

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    /**
     * 发出一条消息
     * @param recipient_id 收件人id
     * @param message 消息内容
     * @param returnUrl 消息发送后返回到的Url
     */
    public static void sendPM(
            Long recipient_id, 
            @Required(message="消息内容不能为空") @MaxSize(message="超过字数限制，请编辑内容后重新提交",value=100)String message,
            String returnUrl)
    {
        User recipient = User.findById(recipient_id);
        // 不能给自己发消息
        if(recipient.id == currentUser.id) return; 
        if(validation.hasErrors()) {
            if(returnUrl == null) {
                returnUrl = "/";
            }
            validation.keep();
            redirect(returnUrl);
        }
        new Message(message, recipient, currentUser).save();
        flash.put("pm_success", "消息发送成功");
        if(returnUrl == null) {
            returnUrl = "/";
        }
        redirect(returnUrl);
    }
    
    
    /**
     * 我的消息
     * @param message_type 消息类型: 收到/发送/通知
     * @param status 消息状态: 已读/未读
     */
    public static void inbox(String message_type, String status) {
        List<Message> private_messages = Message.find("to = ? order by time desc", currentUser).fetch();
        render(private_messages);
    }
    
    public static void outbox(String message_type, String status) {
        List<Message> private_messages = Message.find("from_id = ? order by time desc", currentUser.id).fetch();
        render(private_messages);
    }
    
    /**
     * 查看消息详情
     * @param id 消息的id
     */
    public static void msgDetails(Long id){
        render();
    }
    
    public static void deleteMessage(Long id){
        Message.delete("byId", id);
    }
    
}
