package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Post;
import models.Rating;
import models.UploadFile;
import models.User;

import org.apache.commons.io.IOUtils;
import org.grouplens.lenskit.GlobalItemRecommender;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;

import play.Logger;
import play.Play;
import play.db.jpa.Blob;
import play.libs.Codec;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import tsmatch.Constants;
import tsmatch.Utils;
import controllers.casino.Check;
import controllers.casino.Secure;
import controllers.casino.Security;

@With(Secure.class)
public class UploadFiles  extends Controller{
    
    private static User currentUser;
    
    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    /**
     * 使用ajax上传一个文件
     * @param qqfile 文件名，来自于ajax-fileuploader，与Tecent没有直接关系
     */
    public static void ajaxUploadOneFile(String qqfile) {
        if(currentUser.credits - 1 < 0) renderJSON("{success: false}");
        currentUser.changeCredits("上传文件 → " + qqfile, -1);
        if (request.isNew) {
            Logger.info("Name of the file %s", qqfile);
            Blob receivedFile = new Blob();
            try {
                InputStream data = request.body;
                receivedFile.set(data, "file");
            } catch (Exception ex) {
                renderJSON("{success: false}");
            }
            currentUser.addFile(qqfile, receivedFile);
            Logger.info("SIZE OF FILES LIST ---- %s", currentUser.files.size());
            Logger.info("UUID of the newUploadedFile %s", currentUser.files.get(currentUser.files.size() - 1).file.getUUID());
            Logger.info("Name of the newUploadedFile %s", currentUser.files.get(currentUser.files.size() - 1).filename);
            renderJSON("{success: true}");
        }
    }
    
    // 上传文件
    public static void upload(){
        renderArgs.put("credits", currentUser.credits);
        renderArgs.put("user", currentUser);
        render();
    }
    
    // 提供下载
    public static void download(Long id){
        UploadFile file = UploadFile.findById(id);
        if(file.owner == currentUser) {
            java.io.InputStream binaryData = file.file.get();
            renderBinary(binaryData, file.filename); 
        }
        if(currentUser.credits - 1 < 0) {
            flash.put("failed", "积分不足");
            showdetail(id);
        } else {
            java.io.InputStream binaryData = file.file.get();
            currentUser.changeCredits("下载文件 → " + file.filename, -1);
            renderBinary(binaryData, file.filename);
        }
    }
    
    // 文件列表
    public static void list(){
        List<UploadFile> files = UploadFile.find("select uploadfile from UploadFile uploadfile " +
                "where uploadfile.owner = ? order by uploadedAt desc", currentUser).from(0).fetch(9);
        render(files);
    }
    
    public static List<Long> getsimilars(Long id, int num){
        Recommender rec = Utils.getRecommender();
        if(rec == null) return new ArrayList();
        GlobalItemRecommender girec = null;
        try {
          girec = rec.getGlobalItemRecommender();
          if(girec == null) return new ArrayList();
          Set<Long> item = new HashSet<Long>();
          item.add(id);
          List<Long> answers = girec.globalRecommend(item,num);
          return answers;
        } finally {
           rec.close();
        }
    }
    
    // 查看文件详情
    public static void showdetail(Long id) {
        notFoundIfNull(id);
        UploadFile showFile = UploadFile.findById(id);
        notFoundIfNull(showFile);

        Rating rating = Rating.find("select rating from Rating rating " +
            "where rating.rater_id = ? and rating.rated_file_id = ?", currentUser.id, id).first();
        renderArgs.put("rating", rating);
        renderArgs.put("file", showFile);
        List<Rating> received_ratings = Rating.find("select rating from Rating rating " +
            "where rating.rated_file_id = ? order by ratedAt desc", showFile.id).from(0).fetch(9);
        renderArgs.put("received_ratings", received_ratings);
        String randomID = Codec.UUID();
        
        List<Long> similarList = getsimilars(id, 5);
        List<UploadFile> similar_files = new ArrayList<UploadFile>();
        for(Long fileid:similarList){
            similar_files.add((UploadFile) UploadFile.findById(fileid));
        }
        render(randomID, similar_files);
    }
    
    public static void editFileInfo(Long id){
        notFoundIfNull(id);
        UploadFile file = UploadFile.findById(id);
        notFoundIfNull(file);
        renderArgs.put("file", file);
        render();
    }

    public static void save(Long id, String filename, String description){
        UploadFile file = UploadFile.findById(id);
        file.filename = filename;
        file.description = description;
        file.save();
        editFileInfo(id);
    }
}
