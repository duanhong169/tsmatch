package controllers;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Comment;
import models.Post;
import models.Rating;
import models.Tag;
import models.UploadFile;
import models.User;

import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.grouplens.lenskit.ItemRecommender;
import org.grouplens.lenskit.ItemScorer;
import org.grouplens.lenskit.Recommender;
import org.grouplens.lenskit.RecommenderBuildException;
import org.grouplens.lenskit.RecommenderEngine;
import org.grouplens.lenskit.baseline.BaselinePredictor;
import org.grouplens.lenskit.baseline.ItemUserMeanPredictor;
import org.grouplens.lenskit.core.LenskitRecommenderEngineFactory;
import org.grouplens.lenskit.data.dao.EventCollectionDAO;
import org.grouplens.lenskit.data.event.SimpleRating;
import org.grouplens.lenskit.knn.item.ItemItemRatingPredictor;
import org.grouplens.lenskit.knn.item.ItemItemRecommender;
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer;
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer;

import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.validation.Required;
import play.db.DB;
import play.db.Model;
import play.db.jpa.Blob;
import play.db.jpa.JPQL;
import play.libs.Codec;
import play.libs.Images;
import play.modules.elasticsearch.ElasticSearch;
import play.modules.elasticsearch.Query;
import play.modules.elasticsearch.search.SearchResults;
import play.modules.paginate.ModelPaginator;
import play.mvc.Before;
import play.mvc.Controller;
import tsmatch.Constants;
import tsmatch.Utils;
import controllers.casino.Security;

public class Application extends Controller {
    
    private static User currentUser;

    // 设置当前连接用户
    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    // 显示首页
    public static void index() {
        //List<Post> findMentorPosts = Post.find("requestType like ? order by postedAt desc", "寻求老师").from(0).fetch(9);
        //List<Post> findMenteePosts = Post.find("requestType like ? order by postedAt desc", "寻求学生").from(0).fetch(9);
        if(currentUser != null){
            renderArgs.put("user", currentUser);
            int num_of_posts = Post.find("byAuthor", currentUser).fetch().size();
            renderArgs.put("num_of_posts", num_of_posts);
            int num_of_files = UploadFile.find("byOwner", currentUser).fetch().size();
            renderArgs.put("num_of_files", num_of_files);
        }
        //List<User> newestMentees = User.find("userType like ? order by createdAt desc", Constants.TSMATCH_USERTYPE_MENTEE).from(0).fetch(9);
        List<User> newestMentors = User.find("userType like ? order by createdAt desc", Constants.TSMATCH_USERTYPE_MENTOR).from(0).fetch(9);
        ModelPaginator newestMentees = new ModelPaginator(User.class, "userType=?", Constants.TSMATCH_USERTYPE_MENTEE).orderBy("createdAt desc");
        render(newestMentees, newestMentors);
    }

    // 返回课程列表
    public static void getCourses() throws SQLException{
        ResultSet rs = DB.executeQuery("select courseName from course");
        List courses = Utils.sqlResultSetToList(rs);
        rs.close();
        renderJSON(courses);
    }
    
    // 返回学历列表
    public static void getLevels() throws SQLException{
        ResultSet rs = DB.executeQuery("select levelName from level");
        List levels = Utils.sqlResultSetToList(rs);
        rs.close();
        renderJSON(levels);
    }
    
    // 返回省份列表
    public static void getProvinces() throws SQLException{
        ResultSet rs = DB.executeQuery("select provinceid, province from provinces");
        List provinces = Utils.sqlResultSetToList(rs);
        rs.close();
        renderJSON(provinces);
    }
    
    // 返回城市列表
    public static void getCities(String provinceid) throws SQLException{
        ResultSet rs = DB.executeQuery("select cityid, city from cities where provinceid ='" + provinceid + "'");
        List cities = Utils.sqlResultSetToList(rs);
        rs.close();
        renderJSON(cities);
    }
    
    // 返回地区列表
    public static void getDistricts(String cityid) throws SQLException{
        ResultSet rs = DB.executeQuery("select areaid, area from areas where cityid ='" + cityid + "'");
        List districts = Utils.sqlResultSetToList(rs);
        rs.close();
        renderJSON(districts);
    }
    
    /**
     * 显示某个用户的头像
     * @param id 用户id
     */
    public static void showAvatar(long id, int type) {
        final User user = User.findById(id);
        notFoundIfNull(user);
        response.setContentTypeIfNotSet(user.avatar.type());
        if(user.avatar.get()==null){
            String avatarPath = Play.applicationPath + File.separator + "public" + File.separator + "images"
                + File.separator + "avatar" + File.separator;
            File defaultAvatar = null;
            switch(type){
                case 80: 
                    defaultAvatar = new File(avatarPath + "default80.png");
                    break;
                case 1: 
                    defaultAvatar = new File(avatarPath + "default.jpg");
                    break;
                case 160:
                    defaultAvatar = new File(avatarPath + "default160.png");
                    break;
                default:
                    defaultAvatar = new File(avatarPath + "default80.png");
                    break;
            }
           renderBinary(defaultAvatar);
        }
        renderBinary(user.avatar.get());
    }
    
    public static void showFileIcon(long id, int type) {
        response.setContentTypeIfNotSet(new Blob().type());
        String iconPath = Play.applicationPath + File.separator + "public" + File.separator + "images"
                + File.separator + "icon" + File.separator;
        File defaultFileIMG = null;
        switch(type){
            case 80: 
                defaultFileIMG = new File(iconPath + "doc80.png");
                break;
            case 160:
                defaultFileIMG = new File(iconPath + "doc160.png");
                break;
            default:
                defaultFileIMG = new File(iconPath + "doc80.png");
                break;
        }
       renderBinary(defaultFileIMG);
    }
    
//    // Yabe
//    public static void show(Long id) {
//        Post post = Post.findById(id);
//        String randomID = Codec.UUID();
//        render(post, randomID);
//    }
//    
//    // Yabe
//    public static void postComment(
//            Long postId, 
//            @Required(message="请输入你的姓名") String author, 
//            @Required(message="评论不能为空") String content,
//            @Required(message="请输入验证码") String code,
//            String randomID) 
//    {
//        Post post = Post.findById(postId);
//        if(!Play.id.equals("test")) {
//            validation.equals(code.toLowerCase(), Cache.get(randomID).toString().toLowerCase()).message("错误的验证码");
//        }
//        if(validation.hasErrors()) {
//            render("Application/show.html", post, randomID);
//        }
//        post.addComment(author, content);
//        flash.success("发布评论成功", author);
//        Cache.delete(randomID);
//        show(postId);
//    }
    
    // 产生验证码
    public static void captcha(String id) {
        Images.Captcha captcha = Images.captcha();
        String code = captcha.getText("#000");
        Cache.set(id, code, "10mn");
        renderBinary(captcha);
    }
    
    // Yabe
//    public static void listTagged(String tag) {
//        List<Post> posts = Post.findTaggedWith(tag);
//        render(tag, posts);
//    }
    
    /**
     * 条件搜索结果页面
     * @param criteria_type 搜索的用户类型
     * @param criteria_course 搜索的课程名称
     * @param criteria_level 搜索的课程年级
     * @param criteria_district 搜索的区域
     */
    public static void conditionSearch(String criteria_type,
            String criteria_course, String criteria_level, String criteria_district){
        List<User> rawResults = new ArrayList<User>();
        QueryBuilder condition = QueryBuilders.matchAllQuery();
        if(!criteria_type.equals("all")) {
            condition = QueryBuilders.filteredQuery(
                    QueryBuilders.matchAllQuery(), 
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("userType", criteria_type))
                    );
        }
        if(!criteria_level.equals("all")) {
            condition = QueryBuilders.filteredQuery(
                    condition, 
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("level", criteria_level))
                    );
        }
        if(!(criteria_district.equals("all"))) {
            condition = QueryBuilders.filteredQuery(
                    condition, 
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("district", criteria_district))
                    );
        }
        Query<User> queryResult = ElasticSearch.query(condition, User.class);
        queryResult.useMapper(true);
        SearchResults<User> resultList = queryResult.fetch();
        
        // 通过elasticsearch搜索得到的User并不包含该Model的所有信息(但是包括id), 因此
        // 这里不能够以这个rawResults为资源来对criteria_course进行过滤, 而是需要先得到
        // realUser, 才能判断是否对某项服务有需求
        rawResults = resultList.objects; 
        Logger.info("criteria_course: (" + criteria_course + ")");
        List<User> results = new ArrayList<User>();
        if(!criteria_course.equals("all")) {
            Logger.info("entering the if statement");
            for(User result : rawResults) {
                User realUser = User.findById(result.id);
                List<String> courses = realUser.getServces();
                if(courses.contains(criteria_course)) results.add(realUser);
            }
        } else {
            results = rawResults;
        }
        render(results);
    }
    
    /**
     * 关键字搜索结果页面
     * 
     * @param query_string 搜索字符串
     * @param filter 搜索过滤标识
     * 
     * @author duanhong169@gmail.com
     */
    public static void keywordSearch(@Required(message="请输入关键字") String query_string, int filter) {
        if(validation.hasErrors()) {
            render();
        }
        
        // 过滤条件代码
        final int MATCH_ALL = 0;
        final int MATCH_STUDENT = 1;
        final int MATCH_TEACHER = 2;
        final int MATCH_ORG = 3;
        final int MATCH_POST = 4;
        
        List<User> resultMentees = new ArrayList<User>();
        List<User> resultMentors = new ArrayList<User>();
        List<User> resultAgencies = new ArrayList<User>();
        List<Post> posts = new ArrayList<Post>();

        Logger.info("filter = " + filter);
        Logger.info("=======================");
        
        if(filter == MATCH_ALL || filter == MATCH_STUDENT){
            Logger.info("query mentee");
            //检索学员用户
            //Query<UserInfo> queryUserInfo = ElasticSearch.query(QueryBuilders.constantScoreQuery(FilterBuilders.numericRangeFilter("postCode").from(0).to(1)), UserInfo.class);
            Query<User> queryMentee = ElasticSearch.query(QueryBuilders.filteredQuery(QueryBuilders.queryString(query_string), 
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("userType", Constants.TSMATCH_USERTYPE_MENTEE))), User.class);
            //Query<UserInfo> queryUserInfo = ElasticSearch.query(QueryBuilders.queryString(query_string), UserInfo.class);
            queryMentee.useMapper(true);//see:  playframework-elasticsearch/issues/42
            SearchResults<User> menteeList = queryMentee.fetch();
            
            //This will not work with the @ElasticSearchEmbeded notation
            //SearchResults<Post> list = ElasticSearch.search(QueryBuilders.queryString(course), Post.class);
            resultMentees = menteeList.objects;
            Logger.info("found mentees: " + resultMentees.size());
        }
        
        if(filter == MATCH_ALL || filter == MATCH_TEACHER){
            Logger.info("query mentor");
            //检索教员用户
            Query<User> queryMentor = ElasticSearch.query(QueryBuilders.filteredQuery(QueryBuilders.queryString(query_string),
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("userType", Constants.TSMATCH_USERTYPE_MENTOR))), User.class);
            queryMentor.useMapper(true);
            SearchResults<User> mentorList = queryMentor.fetch();
            resultMentors = mentorList.objects;
            Logger.info("found teachers: " + resultMentors.size());
        }
        
        if(filter == MATCH_ALL || filter == MATCH_ORG){
            Logger.info("query agency");
            //检索机构用户
            Query<User> queryAgency = ElasticSearch.query(QueryBuilders.filteredQuery(QueryBuilders.queryString(query_string),
                    FilterBuilders.queryFilter(QueryBuilders.termQuery("userType", Constants.TSMATCH_USERTYPE_AGENCY))), User.class);
            queryAgency.useMapper(true);
            SearchResults<User> agencyList = queryAgency.fetch();
            resultAgencies = agencyList.objects;
            Logger.info("found agencies: " + resultAgencies.size());
        }
        
        if(filter == MATCH_ALL || filter == MATCH_POST){
            //检索Post
            Query<Post> queryPost = ElasticSearch.query(QueryBuilders.queryString(query_string), Post.class);
            queryPost.useMapper(true);
            SearchResults<Post> postList = queryPost.fetch();
            posts = postList.objects;
        }
        
        Logger.info("=======================");
        
        render(resultMentees, resultMentors, resultAgencies, posts);
    }

    // 重新索引所有model
//    public static void reindexAll(){
//        ElasticSearch.client().admin().indices()
//            .prepareDelete("_all")
//            .execute().actionGet();
//        
//        Utils.customizeModels();
//        updateModelIndex(Tag.class);
//        updateModelIndex(Rating.class);
//        updateModelIndex(UploadFile.class);
//        updateModelIndex(Comment.class);
//        updateModelIndex(Post.class);
//        updateModelIndex(User.class);
//        String url = flash.get("url");
//        if(url == null) {
//            url = "/";
//        }
//        redirect(url);
//    }
//    
//    // 根据JPA中对象重新建立索引
//    private static <T extends Model> void updateModelIndex(Class<T> clazz) {
//
//        List<T> objs = JPQL.instance.findAll(clazz.getName());
//        for (T obj : objs) {
//            ElasticSearch.index(obj);
//        }
//    }
    
}