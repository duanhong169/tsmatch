package controllers;


import java.util.List;

import models.CreditLog;
import models.Message;
import models.Relation;
import models.User;
import play.Logger;
import play.cache.Cache;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;
import tsmatch.Constants;
import controllers.casino.Secure;
import controllers.casino.Security;

@With(Secure.class)
public class Relations extends Controller {
    
    private static User currentUser;

    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    public static void denyMatch(long id){
        User user = User.findById(id);
        Cache.delete("matchlist_" + id);
        Cache.delete("matchlist_" + currentUser.id);
        if(currentUser.userType.equals(user.userType)) return;
        if(currentUser.userType.equals(Constants.TSMATCH_USERTYPE_MENTOR)){
            Relation relation = Relation.find("select relation from Relation relation where relation.mentor = ? " +
            		"and relation.mentee = ?", currentUser, user).first();
            if(relation == null) new Relation(currentUser, user, 0).save();
            else {
                relation.relationship = 0;
            }
        }else if(currentUser.userType.equals(Constants.TSMATCH_USERTYPE_MENTEE)){
            Relation relation = Relation.find("select relation from Relation relation where relation.mentee = ? " +
                    "and relation.mentor = ?", currentUser, user).first();
            if(relation == null) new Relation(user, currentUser, 0).save();
            else {
                relation.relationship = 0;
            }
        }
        redirect("/user_center");
    }
    
}
