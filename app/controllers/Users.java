package controllers;
 
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import models.CreditLog;
import models.Post;
import models.Rating;
import models.Tag;
import models.User;

import controllers.casino.Check;
import controllers.casino.Secure;
import controllers.casino.Security;

import play.*;
import play.data.binding.Binder;
import play.db.Model;
import play.exceptions.TemplateNotFoundException;
import play.libs.Codec;
import play.mvc.*;
import tsmatch.Constants;

@With(Secure.class)
public class Users extends Controller {

    private static User currentUser;
    
    @Before
    static void setConnectedUser() {
        if(Security.isConnected()) {
            currentUser = User.find("byEmail", Security.connected()).first();
            renderArgs.put("user", currentUser);
        }
    }
    
    // 我的资料
    public static void myInfo() {
        renderArgs.put("user", currentUser);
        render();
    }
    
    // 查看其他用户的资料
    public static void showInfo(Long id) {
        notFoundIfNull(id);
        User showUser = User.findById(id);
        notFoundIfNull(showUser);
        
        if(showUser.equals(currentUser)){
            myInfo();
        }

        Rating rating = Rating.find("select rating from Rating rating " +
            "where rating.rater_id = ? and rating.rated_user_id = ?", currentUser.id, id).first();
        renderArgs.put("rating", rating);
        renderArgs.put("user", showUser);
        List<Rating> received_ratings = Rating.find("select rating from Rating rating " +
            "where rating.rated_user_id = ? order by ratedAt desc", id).from(0).fetch(9);
        renderArgs.put("received_ratings", received_ratings);
        String randomID = Codec.UUID();
        render(randomID);
    }
    
    /**
     * 列出某个类型的用户
     * @param usertype 列出的用户类型
     * @param page 按页提取用户数据, 只向服务器请求需要的部分数据, 否则会造成性能问题
     * @param orderBy 用户排列的规则
     * @param order 排列顺序
     */
    public static void listUsersOfType(String usertype, int page, String orderBy, String order){
        if (page < 1) {
            page = 1;
        }
        // TODO: 用户的分页显示
        List<User> users = User.find("userType = ? order by username asc", usertype).fetch();
        render(users, usertype);
    }
    
    /**
     * 保存用户对资料的编辑
     * @param services 用户所关系的服务(配对需求)
     */
    public static void save(String username, String gender, String age, String address, 
            String userType, String level, String toHouse, String services){

        Logger.info(services);
        currentUser.username = username;
        currentUser.gender = gender.equals("男") ? 1 : 0;
        currentUser.age = Integer.parseInt(age);
        currentUser.address = address;
        currentUser.userType = userType;
        currentUser.level = level;
        currentUser.toHouse = toHouse.equals("是");
        for(String service : services.split("\\s+")) {
            if(service.trim().length() > 0) {
                currentUser.addService(service);
            }
        }
        currentUser._save();
        
        flash.success(play.i18n.Messages.get("information_saved"));
        redirect(request.controller + ".myInfo");
    }
    
    // 上传头像页面
    public static void uploadAvatar(){
        renderArgs.put("user", currentUser);
        render();
    }
    
    // 上传头像的方法
    public static void updateAvatar(User user) {
        user.save();
        uploadAvatar();
    }
    
    // 设置地理位置
    public static void chooseLocation() {
        String currentGeoPoint = currentUser.geoPoint;
        render(currentGeoPoint);
    }
    
    // 更新地理位置
    public static void updateLocation(String geoPosition) {
        currentUser.geoPoint = geoPosition;
        currentUser.save();
        chooseLocation();
    }
    
    // 我的积分
    public static void credits() {
        List<CreditLog> credit_logs = CreditLog.find("owner = ? order by time desc", currentUser).fetch();
        render(credit_logs);
    }

    // 获取当前用户的积分
    public static void getCredits(){
        long credits = currentUser.credits;
        renderJSON(credits);
    }
    
    public static void updatepassword() {
        render();
    }
    
    public static void blacklist() {
        render();
    }
    
}