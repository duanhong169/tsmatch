package functionaltests;
import java.util.List;

import models.Message;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class UsersTest extends FunctionalTest {

    @Before
    public void prepare(){
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
    }
    
    @Test
    public void testShowMyInfo() {
        Response response = GET("/myinfo");
        assertStatus(200, response);
        assertThat(renderArgs("user"), is(notNullValue()));
        User user = (User) renderArgs("user");
        assertThat(user.username, is("段弘"));
        response = GET("/user/4");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/myinfo", response);
    }
    
    @Test
    public void testShowUserInfo() {
        Response response = GET("/user/2");
        assertStatus(200, response);
        assertThat(renderArgs("user"), is(notNullValue()));
        User user = (User) renderArgs("user");
        assertThat(user.username, is("林林"));
    }
    
    @Test
    public void testEditMyInfo() {
        
    }
    
    @Test
    public void testUploadAvatar() {
        
    }
    
    @Test
    public void testChooseLocation() {
        
    }
    
    @Test 
    public void testCreditLog() {
        Response response = GET("/credits");
        assertStatus(200, response);
        assertThat(renderArgs("credit_logs"), is(notNullValue()));
    }
    
}