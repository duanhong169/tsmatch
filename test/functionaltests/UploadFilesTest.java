package functionaltests;
import java.util.List;

import models.Message;
import models.UploadFile;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.Ratings;
import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class UploadFilesTest extends FunctionalTest {

    @Before
    public void prepare(){
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
    }
    
    @Test
    public void testUploadFile() {
    }
    
    @Test
    public void testUploadFileList() {
        GET("/list_my_files");
        List<UploadFile> files = (List<UploadFile>) renderArgs("files");
        assert files.size() > 0;
    }
    
    @Test
    public void testShowFileDetail(){
        Response response = GET("/file/1");
        assertStatus(200, response);
        UploadFile file = (UploadFile) renderArgs("file");
        assertNotNull(file);
    }
    
}