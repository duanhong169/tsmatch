package functionaltests;
import java.util.List;

import models.Message;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class MessagesTest extends FunctionalTest {

    @Test
    public void testMsgListSecurity() {
        Response response = GET("/inbox");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/login", response);
    }
    
    @Test
    public void testMsgList() {
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
        Response response = GET("/inbox");
        assertStatus(200, response);
        assertThat(renderArgs("private_messages"), is(notNullValue()));
        List<Message> messages = (List<Message>) renderArgs("private_messages");
        assert messages.size() > 0;
        response = GET("/outbox");
        assertStatus(200, response);
        assertThat(renderArgs("private_messages"), is(notNullValue()));
        messages = (List<Message>) renderArgs("private_messages");
        assert messages.size() > 0;
    }
    
    @Test
    public void testMsgSend(){
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
        Response response = GET("/outbox");
        assertStatus(200, response);
        List<Message> messages = (List<Message>) renderArgs("private_messages");
        long beforesize = messages.size();
        POST("/send_message?recipient_id=2&message=test%20message%20from%20duanhong");
        response = GET("/outbox");
        assertStatus(200, response);
        messages = (List<Message>) renderArgs("private_messages");
        long aftersize = messages.size();
        assertEquals(beforesize, aftersize - 1);
    }
    
}