package functionaltests;
import java.util.List;

import models.Message;
import models.Rating;
import models.UploadFile;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.Ratings;
import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class RatingsTest extends FunctionalTest {

    @Before
    public void prepare(){
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
    }
    
    @Test
    public void testRateFile() {
        Response response = POST("/rate?fileId=1&content=good%20test169&score=4");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/file/1", response);
        List<Rating> ratings = Rating.find("byContent", "good test169").fetch();
        for(Rating rating : ratings){
            rating._delete();
        }
    }
    
    @Test
    public void testRatingList() {
        Response response = GET("/file/1");
        assertStatus(200, response);
        List<Ratings> ratings = (List<Ratings>) renderArgs("received_ratings");
        assert ratings.size() > 0;
    }
    
    @Test
    public void testRatingDisplay(){
        Response response = GET("/file/1");
        assertStatus(200, response);
        UploadFile file = (UploadFile) renderArgs("file");
        assert file.rating_average >= 0 && file.rating_average <=5;
    }
    
}