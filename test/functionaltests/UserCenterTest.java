package functionaltests;
import java.util.List;

import models.UploadFile;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class UserCenterTest extends FunctionalTest {

    @Test
    public void testUserCenterSecurity() {
        Response response = GET("/user_center");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/login", response);
    }
    
    @Test
    public void testMatchList() {
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
        Response response = GET("/user_center");
        assertStatus(200, response);
        assertThat(renderArgs("matched_users"), is(notNullValue()));
        List<User> users = (List<User>) renderArgs("matched_users");
        if(users.size()==0) {
            sleep(1000);
            response = GET("/user_center");
            users = (List<User>) renderArgs("matched_users");
        }
        assertEquals(1,users.size());
    }
    
    @Test
    public void testRecList() {
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
        Response response = GET("/user_center");
        assertStatus(200, response);
        assertThat(renderArgs("rec_files"), is(notNullValue()));
        List<UploadFile> files = (List<UploadFile>) renderArgs("rec_files");
        if(files.size()==0) {
            sleep(1000);
            response = GET("/user_center");
            files = (List<UploadFile>) renderArgs("rec_files");
        }
        assertEquals(8,files.size());
    }
}