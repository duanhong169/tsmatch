package functionaltests;
import models.User;

import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.*;

import controllers.casino.Secure;
import static org.hamcrest.core.Is.is;

import play.mvc.Http.Response;
import play.mvc.results.Redirect;
import play.test.FunctionalTest;

public class ApplicationTest extends FunctionalTest {

    @Before
    public void prepare(){
    }
    
    @Test
    public void testThatIndexPageWorks() {
        Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }

    @Test
    public void testUserLoginAndLogout() {
        Response response = GET("/user_center");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/login", response);
        POST("/login?username=duanhong169@gmail.com&password=duanhong");
        response = GET("/user_center");
        assertThat(renderArgs("user"), is(notNullValue()));
        User user = (User) renderArgs("user");
        assertThat(user.username, is("段弘"));
        assertStatus(200, response);
        response = GET("/logout");
        assertStatus(302, response);
        assertHeaderEquals("Location", "/login", response);
    }
    
    @Test
    public void testUserAvatar(){
        Response response = GET("/user/4/avatar/80");
        assertEquals("inline; filename=\"default80.png\"", response.getHeader("Content-Disposition"));
        response = GET("/user/4/avatar");
        assertEquals("inline; filename=\"default80.png\"", response.getHeader("Content-Disposition"));
        response = GET("/user/4/avatar/1");
        assertEquals("inline; filename=\"default.jpg\"", response.getHeader("Content-Disposition"));
        response = GET("/user/4/avatar/160");
        assertEquals("inline; filename=\"default160.png\"", response.getHeader("Content-Disposition"));
    }
    
    @Test
    public void testFileIcon(){
        Response response = GET("/file/1/icon/80");
        assertEquals("inline; filename=\"doc80.png\"", response.getHeader("Content-Disposition"));
        response = GET("/file/1/icon");
        assertEquals("inline; filename=\"doc80.png\"", response.getHeader("Content-Disposition"));
        response = GET("/file/1/icon/160");
        assertEquals("inline; filename=\"doc160.png\"", response.getHeader("Content-Disposition"));
    }
}