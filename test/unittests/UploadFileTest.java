package unittests;
import java.util.List;
import java.util.Map;

import models.Comment;
import models.Post;
import models.Tag;
import models.UploadFile;
import models.User;

import org.junit.Before;
import org.junit.Test;

import controllers.casino.Security;

import casino.Casino;

import play.Logger;
import play.db.jpa.Blob;
import play.test.Fixtures;
import play.test.UnitTest;

public class UploadFileTest extends UnitTest {

	@Before
    public void setup() {

    }
	
    @Test
    public void createAndRetrieveFile() {
        UploadFile testfile = UploadFile.findById(1L);
        Blob testblob = testfile.file;
        User duanhong = User.find("byEmail", "duanhong169@gmail.com").first();
        UploadFile testFile = new UploadFile(duanhong, "测试文件from test", testblob).save();
        List<UploadFile> testFiles = UploadFile.find("byOwner", duanhong).fetch();
        assertNotNull(testFiles);
        boolean testFileExist = false;
        for(UploadFile file:testFiles){
            if(file.filename.equals("测试文件from test")) testFileExist = true;
        }
        assertTrue(testFileExist);
        testFile._delete();
    }
    
    @Test
    public void deleteFile() {
        User duanhong = User.find("byEmail", "duanhong169@gmail.com").first();
        UploadFile testfile = UploadFile.findById(1L);
        Blob testblob = testfile.file;
        new UploadFile(duanhong, "测试文件from test", testblob).save();
        List<UploadFile> testFiles = UploadFile.find("byOwner", duanhong).fetch();
        assertNotNull(testFiles);
        long counter = testFiles.size();
        for(UploadFile file:testFiles){
            Logger.info(file.filename);
            if(file.filename.equals("测试文件from test")) file._delete();
        }
        assertEquals(counter-1, UploadFile.find("byOwner", duanhong).fetch().size());
    }
}
