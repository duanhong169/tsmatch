package unittests;
import java.util.List;
import java.util.Map;

import models.Comment;
import models.Post;
import models.Tag;
import models.User;

import org.junit.Before;
import org.junit.Test;

import controllers.casino.Security;

import casino.Casino;

import play.test.Fixtures;
import play.test.UnitTest;

public class UserTest extends UnitTest {

	@Before
    public void setup() {
	    User testUser = User.find("byEmail", "test@gmail.com").first();
	    if(testUser!=null) testUser._delete();
    }
	
    @Test
    public void createAndRetrieveUser() {
        String pwHash = Casino.getHashForPassword("secret");
        new User("test@gmail.com", "测试账号", pwHash, "").save();
        User testUser = User.find("byEmail", "test@gmail.com").first();
        assertNotNull(testUser);
        assertEquals("测试账号", testUser.username);
    }
    
    @Test
    public void tryUserAuth() {
        String pwHash = Casino.getHashForPassword("secret");
        new User("test@gmail.com", "测试账号", pwHash, "").save();
        assertFalse(Security.authenticate("test@gmail.com", "wrongpw"));
        assertFalse(Security.authenticate("test_wrongun@gmail.com", "secret"));
        assertTrue(Security.authenticate("test@gmail.com", "secret"));
    }
    
    public void editUser(){
        
    }
}
