slidBox = function () {};
$.extend(slidBox, {
	prototype : {
		init : function (imgIDArr, intervalTime, slidTime) {
			this.imgIDArr = imgIDArr;
			this.imgCount = imgIDArr.length;
			this.currentImgIndex = 0;
			this.intervalTime = intervalTime;
			this.slidTime = slidTime;
			this.sliding = true;
			var pointer = this;
			$.each(this.imgIDArr, function (i, item) {
				if (i != pointer.currentImgIndex) {
					$('#' + item).css('opacity', 0.1);
					$('#' + item).css("left", '1000px');
				}
			});
		},
		slidImg : function () {
			if (this.imgCount < 2)
				return;
			frontImgIndex = this.currentImgIndex;
			backImgIndex = this.currentImgIndex + 1;
			if (backImgIndex >= this.imgCount) {
				$("#icon0" + (this.imgCount)).removeClass("current");
				backImgIndex = 0;
			}
			var _backImg = $('#' + this.imgIDArr[backImgIndex]);
			var _frontImg = $('#' + this.imgIDArr[frontImgIndex]);
			_frontImg.fadeTo(500, 0.1, function () {
				_frontImg.css("left", '1000px');
				_backImg.css("left", "0px");
				_backImg.fadeTo(500, 1);
				$("#icon0" + backImgIndex).removeClass("current");
				$("#icon0" + (backImgIndex + 1)).addClass("current");
			});
			this.currentImgIndex = backImgIndex;
		},
		run : function () {
			this.intervalID = setInterval("slidBox1.slidImg()", this.intervalTime);
			this.sliding = true;
		},
		clickSlid : function (clickIndex) {
			clearInterval(this.intervalID);
			this.sliding = false;
			if (clickIndex < -1 || clickIndex > this.imgCount - 1)
				return;
			frontImgIndex = this.currentImgIndex;
			backImgIndex = clickIndex;
			var _backImg = $('#' + this.imgIDArr[backImgIndex]);
			var _frontImg = $('#' + this.imgIDArr[frontImgIndex]);
			_frontImg.fadeTo(1, 0.1, function () {
				_frontImg.css("left", '1000px');
				_backImg.css("left", "0px");
				_backImg.fadeTo(500, 1);
				$("#icon0" + (frontImgIndex + 1)).removeClass("current");
				$("#icon0" + clickIndex).addClass("current");
			});
			this.currentImgIndex = clickIndex;
			clickIndex += 1;
			this.stopSlid("slidPause");
		},
		stopSlid : function (pauseId) {
			clearInterval(this.intervalID);
			this.sliding = false;
			$('#' + pauseId).removeClass('pause');
			$('#' + pauseId).addClass('play');
		}
	}
});
var slidBox1 = new slidBox();

