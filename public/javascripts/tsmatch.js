//搜索框获得焦点
function searchFocus() {
	var $this = $("#search_query");
	if ($this.val() == $this.attr('data-original-title')) {
		$this.val("");
		$("#search_btn").addClass("typing");
		$this.parent().addClass("highlight");
	} else if ($this.val() !== "" && $this.val() !== $this.attr('title')) {
		$("#search_btn").addClass("typing");
		$this.parent().addClass("highlight");
	}
}
//搜索框失去焦点
function searchBlur() {
	var $this = $("#search_query");
	if ($this.val() == "") {
		$this.val($this.attr('data-original-title'));
		$("#search_btn").removeClass("typing");
		$this.parent().removeClass("highlight");
	} else {
		$("#search_btn").removeClass("typing");
		$this.parent().removeClass("highlight");
	}
} 

//client端验证表单
function validateRatingForm(){
	var score = document.forms["ratingForm"]["score"].value;
	if (score == null || score == "0"){
		var notice = $('form[name = "ratingForm"] span.j-rateNotice');
  		notice.removeClass('hidden');
		return false;
  	}
	var content = document.forms["ratingForm"]["content"];
	if (content.value == null || content.value == ""){
		content.value = content.placeholder;
	}
}

//点击评分条时修改input
function changeForm(form, num) {
	var texts = {
		"1" : "\u5f88\u5dee",
		"2" : "\u4e0d\u592a\u597d",
		"3" : "\u4e00\u822c",
		"4" : "\u4e0d\u9519",
		"5" : "\u975e\u5e38\u597d"
	},
	textarea = form.find('textarea[name = "content"]'),
	notice = form.find('span.j-rateNotice'),
	value = textarea.value;
	form.find('input[name="score"]').attr('value', num);
	if (!value || value == textarea.attr("placeholder")) {
		textarea.value = texts[num]
	}
	textarea.attr("placeholder", texts[num]);
	if (notice) {
		notice.remove();
	}
	textarea.focus();
	textarea.blur()
}

//file input 选择后反馈
function fileInputs() {
  var $this = $(this),
      $val = $this.val(),
      valArray = $val.split('\\'),
      newVal = valArray[valArray.length-1],
      $button = $this.siblings('.button'),
      $fakeFile = $this.siblings('.file-holder');
  if(newVal !== '') {
    $button.text('已选择图片');
    if($fakeFile.length === 0) {
      $button.after('' + newVal + '');
    } else {$fakeFile.text(newVal);}
  }
};

// 其他分站
function showSyArea(){
	$("#sy_area_span").show();
}

function hideSyArea(){
	$("#sy_area_span").hide();
}

// 条件搜索
function condition_search(){
    
    var url_ = "/condition_search?criteria_type=" + $("#criteriaType").val();
    url_ += "&criteria_course=" + $("#criteriaCourse").val();
    url_ += "&criteria_level=" + $("#criteriaLevel").val();
    if ($("#criteriaCity").val() != "0") {
        url_ += "&criteria_city=" + $("#criteriaCity").val();
    } else {
        url_ += "&criteria_city=" + "510100";
    }
    if ($("#criteriaDistrict").val() != "0") {
        url_ += "&criteria_district=" + $("#criteriaDistrict").val();
    } else {
        url_ += "&criteria_district=" + "510110";
    }
    // if($("#hasPhoto_head").attr("checked")){
             // url_+="&&search.sb.havingpic="+$("#hasPhoto_head").val();
    // }
    window.open(url_, '_self');
}

//获取课程列表
function get_courses(selValue) {
    $.ajax({
        url: '/courses', 
        dataType : "json",
        data : "{}",
        textField : "courseName",
        valueField : "courseName",
        contentType : "application/json;charset=utf-8",
        traditional : true,
        success : function(data) //成功后执行的语句, 这里是一个函数, "data"是返回的数据
        {
            var option = "<option value='all' selected='selected'>不限</option>"; 
            $.each(data, function (i, item) { //遍历所得到的JSON
                var selected = '';
                if (item.courseName == selValue) {
                    selected = 'selected="selected"';
                }
                option += "<option value='" + item.courseName + "'" + selected + ">" 
                          + item.courseName + "</option>";
            });
            $("#criteriaCourse").html(option); //填充下拉框选项
        }
    });
}

//获取学历列表
function get_levels(selValue) {
    $.ajax({
        url: '/levels', 
        dataType : "json",
        data : "{}",
        textField : "levelName",
        valueField : "levelName",
        contentType : "application/json;charset=utf-8",
        traditional : true,
        success : function(data) //成功后执行的语句, 这里是一个函数, "data"是返回的数据
        {
            var option = "<option value='all' selected='selected'>不限</option>"; 
            $.each(data, function (i, item) { //遍历所得到的JSON
                var selected = '';
                if (item.levelName == selValue) {
                    selected = 'selected="selected"';
                }
                option += "<option value='" + item.levelName + "'" + selected + ">" 
                          + item.levelName + "</option>";
            });
            $("#criteriaLevel").html(option); //填充下拉框选项
        }
    });
}

//获取城市列表
function get_cities(selValue) {
    $.ajax({
        url: '/cities/510000', //通过Ajax取数据的目标页面, 510000是四川的代码, 待扩展
        dataType : "json",
        data : "{}",
        textField : "city",
        valueField : "cityid",
        contentType : "application/json;charset=utf-8",
        traditional : true,
        success : function(data) //成功后执行的语句, 这里是一个函数, "data"是返回的数据
        {
            var option = "<option value='all' selected='selected'>不限</option>"; 
            $.each(data, function (i, item) { //遍历所得到的JSON
                var selected = '';
                if (item.cityid == selValue) {
                    selected = 'selected="selected"';
                }
                option += "<option value='" + item.cityid + "'" + selected + ">" 
                          + item.city + "</option>";
            });
            $("#criteriaCity").html(option); //填充下拉框选项
        }
    });
}

// 加载地区列表
function get_districts(cityid, selValue) {
    var select_district = document.getElementById("select_district");
    select_district.style.visibility="visible";
    $.ajax({
        url: '/application/getdistricts?cityid=' + cityid, //通过Ajax取数据的目标页面
        dataType : "json",
        data : "{}",
        textField : "area",
        valueField : "areaid",
        contentType : "application/json;charset=utf-8",
        traditional : true,
        success : function(data) //成功后执行的语句, 这里是一个函数, "data"是返回的数据
        {
            var option = "<option  value=\"all\" selected=\"selected\">不限</option>"; //遍历所得到的JSON
            $.each(data, function (i, item) {
                var selected = '';
                if (item.areaid == selValue) {
                    selected = 'selected="selected"';
                }
                option += "<option value='" + item.areaid + "'" + selected + ">" + item.area + "</option>";
            });
            $("#criteriaDistrict").html(option); //填充下拉框选项
        }
    });
}

// 当DOM loaded时加载
$(function() {
	//积分弹出框
	$('#level_show_index').mouseover(function(){
		$(this).find('a').attr('title', '');
		$('#index_level_tip').show();
	}).mouseout(function(){
		$('#index_level_tip').hide();
	});
	
	$('.task_award').tooltip({
      selector: "span[rel=tooltip]",
	  placement: "bottom"
    });

	// $('input[title]').tooltip({placement: "bottom"});
	// $('a[title]').tooltip({
	  // placement: "bottom"
    // });
	
	//star rating
	$('.j-star a').click(function(event){
		event.preventDefault();
		var num;
		num = $(this).html();
		var span = $(this).parent().children().first();
		span.attr('style', "width:" + (num * 14 - 2) + "px");
		form = $('form[name="ratingForm"]');
		changeForm(form, num)
	});
	
	//弹出验证码
    $('form[name=ratingForm] textarea[name="content"]').focus(function() { 
        var checkcode = $('form[name="ratingForm"] p.j-checkcode');
  		checkcode.removeClass('hidden');
    }); 
    
	//file input
	$('.file-wrapper input[type=file]').bind('change focus click', fileInputs);
	
	//菜单展开与收起
	$('.menu-control').click(function(){
		if($(this).attr("class")=="opened"){
			$(this).attr("class","closed");
			$(this).find("i").attr("class","icon-chevron-down");
		}else{
		  	$(this).attr("class","opened");
			$(this).find("i").attr("class","icon-chevron-right");
		}
	});
	
	//地区加载
    $('#criteriaCity').change(function(){get_districts(this.value)});
    
    $('.popup-marker').popover({
        html: true,
        trigger: 'manual'
    })
    
});